#ifndef PROCESS_H
#define PROCESS_H

void get_name_by_pid(int pid,char *task_name);
void get_pid_by_name(pid_t *pid, char *task_name);
int get_pid_from_file(char *path);
#endif