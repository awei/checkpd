#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "process.h"


void get_name_by_pid(int pid,char *task_name)
{
	char proc_pid_path[256] = "";
    char buf[256] = "";
 
	sprintf(proc_pid_path,"/proc/%d/status", pid);
	FILE *fp = fopen(proc_pid_path, "r");
	if(fp != NULL)
	{
		if(fgets(buf, 255, fp) == NULL )
		{
			fclose(fp);
			return;
		}
		fclose(fp);
		sscanf(buf, "%*s %s", task_name);
	}
}
void get_pid_by_name(pid_t *pid, char *task_name)
{
	DIR *dir;
	struct dirent *ptr;
	FILE *fp;
	char filepath[50];
	char cur_task_name[50];
	char buf[256];
	dir = opendir("/proc"); 
	if (NULL != dir)
	{
		while ((ptr = readdir(dir)) != NULL)
		{
			if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0))
				continue;
			if (DT_DIR != ptr->d_type)
				continue;
			
			sprintf(filepath, "/proc/%s/status", ptr->d_name);
			fp = fopen(filepath, "r");
			if (NULL != fp)
			{
				if( fgets(buf, 255, fp)== NULL ){
					fclose(fp);
				continue;
			}
			sscanf(buf, "%*s %s", cur_task_name);

			if (!strcmp(task_name, cur_task_name))
			{
				sscanf(ptr->d_name, "%d", pid);
			}
			fclose(fp);
			}
		}
		closedir(dir);
	}
}
int get_pid_from_file(char *path)
{
	char buf[32];
	FILE *fp;
	char *p = NULL;

	if((fp = fopen(path,"r")) == NULL)
	{
		return 0;
	}
	if(fgets(buf,32,fp) != NULL)
	{
		sscanf(buf,"%s",p);
	}
	fclose(fp);
	return atol(buf);
}
