#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "util.h"

int is_num(char *str)
{
	int i=0,len=0,flag=0;
	char *p=NULL;
	
	p=str;
	if(p==NULL)
		return flag;
	
	len=strlen(str);
	for(i=0;i<len;i++)
	{
		if(isdigit(*p))
		{
			p++;
			continue;
		}
		else
		{
			flag=1;
			break;
		}
	}
	return flag?0:1;
}

int num_num(int num)
{
	int number=1;
	int t=0;
	
	t = num/10;
	while(t>0){
		number++;
		t = t/10;
	}
	
	return number;
}