#include <stdio.h>
#include <time.h>
#include "tell_time.h"

void get_time()
{
	time_t timep = 0;
	struct tm *tm = NULL;
	
	timep = time(NULL);
	tm = localtime(&timep);
	printf("%d-%d-%d %d:%d:%d\n", (1900 + tm -> tm_year), ( 1 + tm -> tm_mon), tm -> tm_mday,
                               (tm -> tm_hour), tm -> tm_min, tm -> tm_sec);
}