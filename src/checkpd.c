#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#include <getopt.h>
#include <dirent.h>
#include "util.h"
#include "tell_time.h"
#include "process.h"

#define ACTION_FLAG 1<<0
#define DEBUG_FLAG 1<<1
#define PID_FILE_FLAG  1<<2
#define GET_NAME_FLAG 1<<3
#define PID_NAME_FLAG 1<<4
#define PID_FLAG 1<<5
#define TIME_FLAG 1<<6

struct variable
{
	int pid;
	int flag;
	int interval;
	char task_name[50];
	char *action;
	char *path;
}var;

void usage()
{
	printf("\t-a action\n");
	printf("\t-d debug\n");
	printf("\t-f pid file path\n");
	printf("\t-g get process name\n");
	printf("\t-h help\n");
	printf("\t-i interval\n");
	printf("\t-n process name\n");
	printf("\t-p pid\n");
	printf("\t-t timestamp\n");			
}

void clear_pid()
{
	remove(var.path);
	var.pid = 0;
}

void print_debug(int flag)
{
	if(flag & DEBUG_FLAG)
	{
		if(flag & TIME_FLAG)
		{
			get_time();
		}
		printf("\tpid = %d\n",var.pid);
		if(flag & GET_NAME_FLAG)
		{
			get_name_by_pid(var.pid,var.task_name);
			printf("\tprocess name:%s\n",var.task_name);
		}
	}
}

pid_t _get_pid(struct variable *pvar)
{
	pid_t pid = 0;
	
	if(pvar->flag & PID_FILE_FLAG)
	{
		pid = get_pid_from_file(pvar->path);
	}
	else if(pvar->flag & PID_NAME_FLAG)
	{
		get_pid_by_name(&pid,pvar->task_name);
		
	}
	else if(pvar->flag & PID_FLAG)
	{
		pid = pvar->pid;
	}
	else 
		pid = -1;
	return pid;
}
void pid_handling()
{
	var.pid = _get_pid(&var);
	if(var.pid == -1)
	{
		printf("\tNo processes to monitor\n");
		exit(1);
	}
	else if(var.pid == 0)
	{
		print_debug(var.flag);
		printf("\tProcess does not exist\n\n");
		if(var.flag & ACTION_FLAG)
			system(var.action);
	}
}

int is_interval(char *str)
{	
	if(str == NULL)
	{
		printf("\tNo time instruction,now interval is five\n\n");
	}
	else if(is_num(str))
	{
		return 1;
	}
	else 
	{
		printf("\tThe time is error,now interval is five\n\n");
		printf("\n");
	}
	return 0;
}
int is_pid(char *str)
{
	if(var.flag & PID_FLAG)
	{	
		if(is_num(str))
		{
			return 1;
		}
		else 
		{
			printf("\tpid error\n");
			exit(1);
		}
	}
}

void handler(int sig)
{
	int s = 0;
	
	pid_handling();
	if(var.pid == 0)
		goto end;
	print_debug(var.flag);
	
	s=kill(var.pid,0);
	
	if(s==0)
		printf("\tProcess exists and we can send it a signal\n\n");
	else 
	{
		if(errno == EPERM)
			printf("\tProcess exists,but we don't have permission to send it a signal\n\n");
		else if(errno==ESRCH)
		{
			clear_pid();
			printf("\tProcess does not exist\n\n");
		}
		else 
			printf("\tkill\n");
	}
end:
	alarm(var.interval);
}

int main(int argc,char *argv[])
{
	int opt;
	char *opt_value = NULL;
	char *str_value = NULL;
	char *path = NULL;
	char *pro_name = NULL;
	time_t t;
	struct tm *tm;
	int option_index = 0;
	struct stat buf;
	
	char *opt_string = "a:df:ghi:n:p:t";
	static struct option long_options[] =
    {  
		{"action", required_argument, NULL, 'a'},
		{"debug", no_argument, NULL, 'd'},
		{"file", required_argument, NULL, 'f'},
		{"help", no_argument, NULL, 'h'},
		{"get-name", no_argument , NULL , 'g'},
		{"interval", required_argument, NULL, 'i'},
		{"name", required_argument, NULL, 'n'},
		{"pid", required_argument, NULL, 'p'},
		{"timestamp", no_argument, NULL, 't'},
        {NULL, 0, NULL, 0},
    }; 
	
	var.interval = 5;
	
	while((opt = getopt_long(argc,argv,opt_string,long_options,&option_index)) != -1)
	{
		switch(opt)
		{
			case 'a':
				var.action = optarg;
				var.flag |= ACTION_FLAG;
				break;
			case 'd':
				var.flag |= DEBUG_FLAG;
				break;
			case 'f':
				var.flag |= PID_FILE_FLAG;
				var.path = optarg;
				break;
			case 'g':
				var.flag |= GET_NAME_FLAG;
				break;
			case 'i':
				opt_value = optarg;
				break;
			case 'n':
				var.flag |= PID_NAME_FLAG;
				strcpy(var.task_name,optarg);
				break;
			case 'p':
				var.flag |= PID_FLAG;
				str_value = optarg;
				break;
			case 't':
				var.flag |= TIME_FLAG;
				break;
			case 'h':
			case '?':
			default:
				usage();
				exit(1);
		}
	}
	
	if(is_interval(opt_value))
		var.interval = atoi(opt_value);
	if(is_pid(str_value))
		var.pid = atoi(str_value);
	signal(SIGALRM,handler);
	alarm(var.interval);
	
	while(1)
	{
		pause();
	}
	
}
