#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <ctype.h>
#include <signal.h>
#include "util.h"

void writed()
{
	FILE *fp;
	char *p;
	*p=getpid();
	if((fp=fopen("inotify.pid","w"))==NULL)
	{
		printf("can not open file\n");
		return;
	}
	if(p!=NULL)
	{
		fprintf(fp,"%d\n",getpid());
	}
	fclose(fp);
	
}

static void sigHandler(int sig)
{
	static int count = 0;
	
	if(sig == SIGINT)
	{
		count++;
		printf("Caught SIGINT (%d)\n",count);
		return;
	}
	printf("Caught SIGQUIT - that's all folks!\n");
	exit(EXIT_SUCCESS);
}
int main(int argc,char *argv[])
{
	
	if(signal(SIGINT,sigHandler) == SIG_ERR)
	{
		printf("signal\n");
		return 0;
	}
	if(signal(SIGQUIT,sigHandler) == SIG_ERR)
	{
		printf("signal\n");
		return 0;
	}
	writed();
	while(1)
	{
		pause();	
	}
}