CC:=gcc
MAKE:=make

CHECKP_DIR:=$(shell pwd)
CHECKP_LIB_DIR:=$(CHECKP_DIR)/lib
CHECKP_SRC_DIR:=$(CHECKP_DIR)/src
CHECKP_TEST_DIR:=$(CHECKP_DIR)/test

SYSTEM_BIN_DIR:=/usr/sbin
SYSTEM_LIB:=/lib

all:checkp_lib checkp_src checkp_test

clean:checkp_lib_clean checkp_src_clean checkp_test_clean

install:checkp_lib_install checkp_src_install checkp_test_install

uninstall:checkp_lib_uninstall checkp_src_uninstall checkp_test_uninstall

checkp_lib:
	$(MAKE) -C $(CHECKP_LIB_DIR)
checkp_lib_clean:
	$(MAKE) -C $(CHECKP_LIB_DIR) clean
checkp_lib_install:
	$(MAKE) -C $(CHECKP_LIB_DIR) install
checkp_lib_uninstall:
	$(MAKE) -C $(CHECKP_LIB_DIR) uninstall

checkp_src:
	$(MAKE) -C $(CHECKP_SRC_DIR)
checkp_src_clean:
	$(MAKE) -C $(CHECKP_SRC_DIR) clean
checkp_src_install:
	$(MAKE) -C $(CHECKP_SRC_DIR) install
checkp_src_uninstall:
	$(MAKE) -C $(CHECKP_SRC_DIR) uninstall

checkp_test:
	$(MAKE) -C $(CHECKP_TEST_DIR)
checkp_test_clean:
	$(MAKE) -C $(CHECKP_TEST_DIR) clean
checkp_test_install:
	$(MAKE) -C $(CHECKP_TEST_DIR) install
checkp_test_uninstall:
	$(MAKE) -C $(CHECKP_TEST_DIR) uninstall

export CC
export CHECKP_LIB_DIR